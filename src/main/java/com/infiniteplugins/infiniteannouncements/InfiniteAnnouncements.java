package com.infiniteplugins.infiniteannouncements;

import com.infiniteplugins.infiniteannouncements.addons.AddonManager;
import com.infiniteplugins.infinitecore.InfinitePlugin;
import com.infiniteplugins.infinitecore.config.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

public final class InfiniteAnnouncements extends InfinitePlugin {

    @Override
    public void onPluginLoad() {

    }

    @Override
    public void onPluginEnable() {

        checkAddonManager();

    }

    @Override
    public void onPluginDisable() {

    }

    @Override
    public void onConfigReload() {

    }

    private void checkAddonManager() {
        AddonManager.ValidationType vt = null;
        try {
            vt = new AddonManager(InetAddress.getLocalHost().getHostAddress()+":"+ Bukkit.getServer().getPort(), "https://infiniteplugins.com/webpanel/verify.php", this).isValid();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        if (vt == AddonManager.ValidationType.VALID) {
            //Create the bossbar




            //Send the Done Message with working Patreon add-ons
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Infinite Announcements &e1.6.0"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Add-ons &4Enabled"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Infinite Announcements has been &4Enabled"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));

        } else if (vt == AddonManager.ValidationType.PAGE_ERROR || vt == AddonManager.ValidationType.URL_ERROR) {
            //Send the Done Message without working Patreon add-ons
            System.out.println("Please contact out support team the add-on checker services are offline");
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Infinite Announcements &e1.6.0"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Infinite Announcements has been &4Enabled"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));

        } else {
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&4Infinite Announcements &e1.6.0"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Infinite Announcements has been &4Enabled"));
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8========================================="));
        }
    }


    @Override
    public List<Config> getExtraConfig() {
        return null;
    }

}
